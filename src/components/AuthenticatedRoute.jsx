import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";

function AuthenticatedRoute({ component: Component, admin = false, ...restOfProps }) {
  const user = useSelector((state) => state.user);

  const isAuthenticated = user !== null;
  const isAdmin = user?.isAdmin;

  return (
    <Route
      {...restOfProps}
      render={(props) => {
        if (!isAuthenticated) return <Redirect to="/login" />;

        if (admin && !isAdmin) return <Redirect to="/" />;

        return <Component {...props} />;
      }}
    />
  );
}

export default AuthenticatedRoute;
