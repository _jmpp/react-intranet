import React, { useState } from "react";
import PropTypes from "prop-types";

function Form(props) {
  const [newUser, setNewUser] = useState(
    props.person || {
      gender: null,
      lastname: null,
      firstname: null,
      email: null,
      phone: null,
      birthdate: null,
      city: null,
      country: null,
      photo: null,
    }
  );

  function setUserObject(property) {
    return (event) => {
      setNewUser({ ...newUser, [property]: event.target.value });
    };
  }

  function onSubmit(event) {
    event.preventDefault();
    props.onValidation(newUser);
  }

  return (
    <form onSubmit={onSubmit}>
      <div className="flex flex-col items-start justify-center m-2 sm:flex-row">
        <label htmlFor="Civilité" className="whitespace-nowrap px-2 pt-2 w-full text-lg text-left cursor-pointer sm:w-52 sm:text-right">
          * Civilité :
        </label>
        <div className="w-full sm:w-96 text-left">
          <select id="Civilité" className="input w-full" onChange={setUserObject("gender")} defaultValue={newUser.gender}>
            <option disabled></option>
            <option value="male">Homme</option>
            <option value="female">Femme</option>
          </select>
        </div>
      </div>

      <FormInput type="text" label="Nom" placeholder="SMITH" onChange={setUserObject("lastname")} defaultValue={newUser.lastname} />
      <FormInput type="text" label="Prenom" placeholder="John" onChange={setUserObject("firstname")} defaultValue={newUser.firstname} />
      <FormInput
        type="email"
        label="Email"
        placeholder="john.smith@email.com"
        onChange={setUserObject("email")}
        defaultValue={newUser.email}
      />
      <FormInput type="password" label="Mot de passe" optional placeholder="(min. 8 caractères)" onChange={setUserObject("password")} />
      <FormInput type="tel" label="Téléphone" placeholder="07-89-01-23-45" onChange={setUserObject("phone")} defaultValue={newUser.phone} />
      <FormInput type="date" label="Date de naissance" onChange={setUserObject("birthdate")} defaultValue={newUser.birthdate} />
      <FormInput type="text" label="Ville" placeholder="Paris" onChange={setUserObject("city")} defaultValue={newUser.city} />
      <FormInput type="text" label="Pays" placeholder="France" onChange={setUserObject("country")} defaultValue={newUser.country} />
      <FormInput
        type="url"
        label="URL de la photo"
        optional
        placeholder="https://"
        onChange={setUserObject("photo")}
        defaultValue={newUser.photo}
      />

      <button type="submit" className="button">
        {props.children}
      </button>
    </form>
  );
}

Form.propTypes = {
  onValidation: PropTypes.func.isRequired,
  person: PropTypes.object,
  children: PropTypes.node.isRequired,
};

const FormInput = ({ label, type, placeholder, optional, onChange, defaultValue }) => {
  return (
    <div className="flex flex-col items-center justify-center m-2 sm:flex-row">
      <label htmlFor={label} className="whitespace-nowrap px-2 w-full text-lg text-left cursor-pointer sm:w-52 sm:text-right">
        {!optional ? "* " : ""}
        {label} :
      </label>
      <input
        className="input w-full sm:w-96"
        type={type}
        id={label}
        required={!optional ?? true}
        placeholder={placeholder}
        onChange={onChange}
        defaultValue={defaultValue}
      />
    </div>
  );
};

export default Form;
