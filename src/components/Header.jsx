import { faList, faNetworkWired, faUser, faSignInAlt, faSignOutAlt, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

function Header() {
  const user = useSelector((state) => state.user);
  return (
    <nav className="flex justify-between items-stretch bg-gradient-to-b from-red-400 via-red-500 to-pink-500 text-white px-4">
      <a href="/" className="font-semibold text-xl block py-2" title="Accueil de l'intranet">
        <FontAwesomeIcon icon={faNetworkWired} />
        {` `}
        Intranet
      </a>

      <ul className="flex">
        {/* Si l'utilisateur est déconnecté : */}
        {!user && (
          <>
            <li>
              <NavLink
                to="/login"
                activeClassName="from-red-500 via-red-600 to-pink-600 shadow-inner"
                className="menu-item"
                title="Connexion à l'intranet"
              >
                <FontAwesomeIcon icon={faSignInAlt} />
                {` `}
                Connexion
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/signup"
                activeClassName="from-red-500 via-red-600 to-pink-600 shadow-inner"
                className="menu-item"
                title="S'inscrire sur l'intranet"
              >
                <FontAwesomeIcon icon={faUserPlus} />
                {` `}
                Inscription
              </NavLink>
            </li>
          </>
        )}

        {/* Si l'utilisateur est connecté : */}
        {user && (
          <>
            <li>
              <NavLink
                to="/list"
                activeClassName="from-red-500 via-red-600 to-pink-600 shadow-inner"
                className="menu-item"
                title="Liste des utilisateurs"
              >
                <FontAwesomeIcon icon={faList} />
                {` `}
                Liste
              </NavLink>
            </li>
            {/* Si l'utilisateur est un ADMIN : */}
            {user.isAdmin && (
              <>
                <li>
                  <NavLink
                    to="/create"
                    activeClassName="from-red-500 via-red-600 to-pink-600 shadow-inner"
                    className="menu-item"
                    title="Ajouter un utilisateur"
                  >
                    <FontAwesomeIcon icon={faUser} />
                    {` `}
                    Ajouter
                  </NavLink>
                </li>
              </>
            )}
            <li className="self-center ml-4">
              <img
                src={user.photo || `https://via.placeholder.com/64?text=${encodeURIComponent(user.firstname[0] + " " + user.lastname[0])}`}
                alt={user.firstname}
                className="rounded-full h-8 w-8"
              />
            </li>
            <li>
              <NavLink
                to="/logout"
                activeClassName="from-red-500 via-red-600 to-pink-600 shadow-inner"
                className="menu-item"
                title="Se déconnecter de l'intranet"
              >
                <FontAwesomeIcon icon={faSignOutAlt} />
                {` `}
                Déconnexion
              </NavLink>
            </li>
          </>
        )}
      </ul>
    </nav>
  );
}

export default Header;
