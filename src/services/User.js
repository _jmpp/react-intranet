import axios from "axios";

const Users = axios.create({
  baseURL: process.env.REACT_APP_API_BASE,
  withCredentials: true,
});

//
Users.defaults.timeout = 2500;

// Default headers
Users.defaults.headers.common["Content-Type"] = "application/json";
// axios.defaults.headers.common['Authorization'] = REACT_APP_BEARER_TOKEN;

Users.interceptors.response.use(
  (response) => response, // En cas de succes, on ne fait rien et on renvoie la réponse
  (error) => Promise.reject(error.response.data) // En cas d'erreur, on renvoie l'erreur du serveur
);

const UserService = {
  // Connexion d'un utilisateur
  login(email, password) {
    return Users.post("/login", { email, password }).then((response) => response.data);
  },

  logout() {
    return Users.post("/logout").then((response) => response.data);
  },

  // Inscription d'un utilisateur
  signup(newUser) {
    return Users.post("/signup", newUser).then((response) => response.data);
  },

  // Récupérer le tableau des utilisateurs depuis le serveur
  fetchAll() {
    return Users.get(`/users`).then((response) => response.data);
  },

  fetchOne(id) {
    return Users.get(`/user/${id}`).then((response) => response.data);
  },

  // Ajouter une nouvelle personne
  add(user) {
    return Users.post(`/users`, {
      gender: user.gender,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      phone: user.phone,
      birthdate: user.birthdate,
      city: user.city,
      country: user.country,
      photo: user.photo,
    }).then((response) => response.data);
  },

  // Supprimer une personne
  remove(userId) {
    return Users.delete(`/user/${userId}`).then((response) => response.data);
  },

  // Modifier une personne
  update(user) {
    return Users.put(`/user/${user.id}`, user).then((response) => response.data);
  },
};

export default UserService;
