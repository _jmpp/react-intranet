import Header from "./components/Header";
import Home from "./views/Home";
import List from "./views/List";
import Create from "./views/Create";
import Edit from "./views/Edit";
import Login from "./views/Login";
import Logout from "./views/Logout";
import Signup from "./views/Signup";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Provider } from "react-redux";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <div id="app">
        <Router>
          <ToastContainer position="bottom-right" />

          <Header />

          <Switch>
            <AuthenticatedRoute path="/" component={Home} exact />
            <AuthenticatedRoute path="/list" component={List} />
            <AuthenticatedRoute path="/create" component={Create} admin />
            <AuthenticatedRoute path="/edit/:id" component={Edit} admin />
            <Route path="/login" component={Login} />
            <Route path="/signup" component={Signup} />
            <AuthenticatedRoute path="/logout" component={Logout} />
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
