import { createStore } from "redux";
import { setUser } from "../actions";
import rootReducer from "../reducers";
import StorageService from "../services/StorageService";

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(() => {
  StorageService.set("user", JSON.stringify(store.getState().user));
});

let user = StorageService.get("user");
user = user ? JSON.parse(user) : null;

store.dispatch(setUser(user));

export default store;
