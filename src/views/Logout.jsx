import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setUser } from "../actions";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import UserService from "../services/User";
import { faSync } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Logout() {
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    UserService.logout()
      .then(({ message }) => {
        dispatch(setUser(null));

        toast.success(message);
        history.push("/");
      })
      .catch((err) => toast.error(err.message));
  }, [dispatch, history]);

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5 text-center">Déconnexion …</h1>
      <div className="text-center mt-2">
        <FontAwesomeIcon icon={faSync} spin size="2x" className="text-red-500" />
      </div>
    </div>
  );
}

Logout.propTypes = {};

export default Logout;
