import React, { useRef, useState } from "react";
import { useDispatch /* useSelector */ } from "react-redux";
import { setUser } from "../actions";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import UserService from "../services/User";

function Login() {
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const history = useHistory();
  // const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  async function login(event) {
    event.preventDefault();

    const email = emailRef.current.value;
    const password = passwordRef.current.value;

    try {
      const { message, user: logguedUser } = await UserService.login(email, password);

      // Sauvegarde l'utilisateur dans le state
      dispatch(setUser(logguedUser));

      toast.success(message);
      history.push("/list");
    } catch (err) {
      setErrorMessage(err.message);
    }
  }

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5 text-center">Connexion</h1>
      <hr className="mb-6" />
      <p>Pour vous connecter à l'intranet, entrez votre identifiant et mot de passe.</p>

      {/* <pre className="text-left">{JSON.stringify(user, null, 2)}</pre> */}

      {errorMessage && <div className="error">{errorMessage}</div>}

      <form action="/login" className="mt-6" onSubmit={login}>
        <div className="flex flex-col items-center justify-center m-2 sm:flex-row">
          <label htmlFor="email" className="whitespace-nowrap px-2 pb-1 w-full text-lg text-left cursor-pointer sm:w-52 sm:text-right">
            Email :
          </label>
          <div className="w-full sm:w-96 text-left">
            <input type="email" ref={emailRef} className="input w-full" id="email" required placeholder="ex: owen.lopez@example.com" />
          </div>
        </div>
        <div className="flex flex-col items-center justify-center m-2 sm:flex-row">
          <label htmlFor="password" className="whitespace-nowrap px-2 pb-1 w-full text-lg text-left cursor-pointer sm:w-52 sm:text-right">
            Mot de passe :
          </label>
          <div className="w-full sm:w-96 text-left">
            <input type="password" ref={passwordRef} className="input w-full" id="password" required />
          </div>
        </div>
        <button type="submit" className="button">
          Connexion
        </button>
      </form>
    </div>
  );
}

Login.propTypes = {};

export default Login;
