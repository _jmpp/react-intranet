import React, { useState } from "react";
import { useHistory } from "react-router";
import { toast } from "react-toastify";

import Form from "../components/Form";
import UserService from "../services/User";

function Signup() {
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState(null);

  async function addUser(newUser) {
    try {
      const { message } = await UserService.signup(newUser);
      toast.success(message);
      history.push("/login");
    } catch (err) {
      setErrorMessage(err.message);
    }
  }

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5 text-center">Inscription à l'intranet</h1>
      <hr className="mb-6" />

      {errorMessage && <div className="error">{errorMessage}</div>}
      <Form onValidation={addUser}>Inscription</Form>
    </div>
  );
}

Signup.propTypes = {};

export default Signup;
