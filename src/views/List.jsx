import { faSync } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { toast } from "react-toastify";
import UserCard from "../components/UserCard";
import UserService from "../services/User";

function List() {
  const [people, setPeople] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [filterName, setFilterName] = useState("name");
  const [errorMessage, setErrorMessage] = useState(null);
  const history = useHistory();
  const user = useSelector((state) => state.user);

  useEffect(() => {
    UserService.fetchAll()
      .then((data) => setPeople(data))
      .catch((err) => setErrorMessage(err.message));
  }, []);

  function editPerson({ id }) {
    history.push(`/edit/${id}`);
  }

  function onSearch(event) {
    setSearchText(event.target.value);
  }

  function changeFilter(event) {
    setFilterName(event.target.value);
  }

  async function deletePerson({ id }) {
    try {
      const {
        removed: { firstname, lastname, gender },
      } = await UserService.remove(id);

      toast.success(`${firstname} ${lastname} a été supprimé${gender === "female" ? "e" : ""} !`);
      const newPeopleArray = people.filter((person) => person.id !== id);
      setPeople(newPeopleArray);
    } catch (err) {
      toast.error(err.message);
    }
  }

  const filteredPeople = people?.filter(getFilter(filterName));

  function getFilter(filterName) {
    switch (filterName) {
      case "name":
        return (person) => (person.firstname + " " + person.lastname).toLowerCase().includes(searchText.toLowerCase());

      case "location":
        return (person) => (person.city + " " + person.country).toLowerCase().includes(searchText.toLowerCase());

      default:
        return (person) => true;
    }
  }

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5 text-center">Liste des collaborateurs</h1>
      <hr className="mb-6" />

      <div className="flex items-center justify-center p-3">
        <input type="search" className="input w-96" placeholder="Recherche …" onInput={onSearch} />

        <p className="whitespace-nowrap m-3">Rechercher par :</p>
        <select className="input w-48" onChange={changeFilter}>
          <option value="name">Nom</option>
          <option value="location">Localisation</option>
        </select>
      </div>

      {errorMessage && <div className="error">{errorMessage}</div>}

      {!people && !errorMessage && (
        <div className="text-center mt-2">
          <FontAwesomeIcon icon={faSync} spin size="2x" className="text-red-500" />
        </div>
      )}

      <div className="mt-8 grid grid-flow-row grid-cols-1 gap-4 lg:grid-cols-2 2xl:grid-cols-3">
        {filteredPeople &&
          filteredPeople.map((person) => (
            <div className="w-full flex justify-center" key={person.id}>
              <UserCard person={person} onDelete={user.isAdmin ? deletePerson : undefined} onEdit={user.isAdmin ? editPerson : undefined} />
            </div>
          ))}
      </div>
    </div>
  );
}

List.propTypes = {};

export default List;
