import React, { useEffect, useState } from "react";

import UserCard from "../components/UserCard";

import UserService from "../services/User";

function Home() {
  const [people, setPeople] = useState(null);
  const [person, setPerson] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {
    UserService.fetchAll()
      .then((data) => {
        setPeople(data);
        setPerson(data[Math.floor(Math.random() * data.length)]);
      })
      .catch((err) => setErrorMessage(err.message));
  }, []);

  function changePerson() {
    setPerson(people[Math.floor(Math.random() * people.length)]);
  }

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5">Bienvenue sur l'intranet</h1>
      <p className="text-xl my-2">La plate-forme de l'entreprise qui vous permet de retrouver tous vos collaborateurs.</p>

      {errorMessage && <div className="error">{errorMessage}</div>}

      {/* Card user */}
      {person && (
        <>
          <p className="text-2xl my-4">Avez-vous dit bonjour à :</p>

          {/* Card user */}
          <div className="w-full flex justify-center">
            <UserCard person={person} />
          </div>

          {/* Button */}
          <button className="button" type="button" onClick={changePerson}>
            Dire bonjour à quelqu'un d'autre
          </button>
        </>
      )}
    </div>
  );
}

Home.propTypes = {};

export default Home;
