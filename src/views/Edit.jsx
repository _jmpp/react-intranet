import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { toast } from "react-toastify";

import Form from "../components/Form";
import UserCard from "../components/UserCard";
import UserService from "../services/User";

function Edit() {
  const { id } = useParams();
  const [person, setPerson] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const history = useHistory();

  useEffect(() => {
    UserService.fetchOne(id).then((personData) => setPerson(personData));
  }, [id]);

  async function editPerson(updatedPerson) {
    try {
      const { message } = await UserService.update(updatedPerson);
      toast.success(message);
      history.push("/list");
    } catch (err) {
      setErrorMessage(err.message);
    }
  }

  return (
    <div className="container p-2 text-center">
      <h1 className="text-5xl font-semibold my-5 text-center">Modifier un collaborateur</h1>
      <hr className="mb-6" />

      {person && (
        <div className="flex justify-center mb-8">
          <UserCard person={person} />
        </div>
      )}

      {errorMessage && <div className="error">{errorMessage}</div>}
      {person && (
        <Form person={person} onValidation={editPerson}>
          Modifier
        </Form>
      )}
    </div>
  );
}

Edit.propTypes = {};

export default Edit;
