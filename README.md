# Environnement de dev

[Visual Studio Code](https://code.visualstudio.com/), avec les extensions suivantes d'installées :

- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Reactjs code snippets](https://marketplace.visualstudio.com/items?itemName=xabikos.ReactSnippets)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)
- [Postcode](https://marketplace.visualstudio.com/items?itemName=rohinivsenthil.postcode)
- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
- [npm Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)

# Initialisation du projet

Pour installer les dépendances pour le client, à la racine du projet, lancer :

```
npm install
```

Pour installer les dépendances pour le serveur, se déplacer dans le dossier `_server` :

```
cd _server
npm install
```

# Lancer le projet

À la racine, lancer le serveur :

```
npm run server
```

À la racine, lancer l'application React :

```
npm run client
```

# Builder l'application

Lancer le build de l'application vers le dossier `/build`

```
npm run build
```

Pour plus de détails, consulter la section [déploiement](https://create-react-app.dev/docs/deployment/) de la documentation.
